package com.example.asus.yupablog;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Paint;
import android.graphics.Typeface;
import android.media.MediaDrm;
import android.net.Uri;
import android.os.Build;
import android.support.annotation.MainThread;
import android.support.annotation.RequiresApi;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Html;
import android.text.Selection;
import android.text.TextUtils;
import android.view.ActionMode;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.theartofdev.edmodo.cropper.CropImage;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import yuku.ambilwarna.AmbilWarnaDialog;

public class YupaBlog extends AppCompatActivity {
    private EditText blogTitle;
    private Spinner blogCategory, blogTFamily;
    private ImageButton btnUploadIMG, btnBold, btnItalic, btnUnderline, btnBulletlist, btnTextColor;
    private Button btnDone;

    ArrayList<String> addArray = new ArrayList<String>();
    private Uri resultUri;
    private EditText blogET;
    private boolean isBullet = false;
    private boolean isUnderline = false;
    private boolean isBold = false;
    private boolean isItalic = false;
    private int defaultColor;
    private static final String NEWLINE_CHAR = "<br />";
    private static final String HTML_BULLETPOINT = "&#8226;";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.blog);

        blogTitle = findViewById(R.id.id_title);
        blogCategory = findViewById(R.id.id_spinner1);
        btnUploadIMG = findViewById(R.id.id_Photo);
        blogTFamily = findViewById(R.id.id_spinner2);
        btnBold = findViewById(R.id.Bold);
        btnItalic = findViewById(R.id.Italic);
        btnUnderline = findViewById(R.id.Underline);
        btnTextColor = findViewById(R.id.Text_Color);
        btnBulletlist = findViewById(R.id.Bulletlist);
        btnDone = findViewById(R.id.btn_done);
        blogET = findViewById(R.id.Text);


        defaultColor = ContextCompat.getColor(YupaBlog.this, R.color.colorPrimary);


        btnUploadIMG.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                CropImage.activity().start(YupaBlog.this);
            }
        });

        blogTFamily.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                switch (position)
                {
                    case 0 :
                        Typeface type = Typeface.createFromAsset(getAssets(),"Font/OpenSans-Regular.ttf");
                        blogET.setTypeface(type);
                        break;
                    case 1 :
                        Typeface type1 = Typeface.createFromAsset(getAssets(),"Font/LatoBlackItalic.ttf");
                        blogET.setTypeface(type1);
                        break;
                    case 2 :
                        Typeface type2 = Typeface.createFromAsset(getAssets(),"Font/Thin.ttf");
                        blogET.setTypeface(type2);
                        break;
                    case 3 :
                        Typeface type3 = Typeface.createFromAsset(getAssets(),"Font/DancingScriptRegular.otf");
                        blogET.setTypeface(type3);
                        break;
                }
            }
            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        btnBold.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int Start = blogET.getSelectionStart();
                int End = blogET.getSelectionEnd();

                String StartingText = blogET.getText().toString().substring(0, Start).replaceAll("\n", NEWLINE_CHAR );
                String SelectionText = blogET.getText().toString().substring(Start, End).replaceAll("\n", NEWLINE_CHAR );
                String EndingText = blogET.getText().toString().substring(End).replaceAll("\n", NEWLINE_CHAR );

                if (!SelectionText.isEmpty()) {

                    if (!isBold) {
                        blogET.setText(Html.fromHtml(StartingText + "<b>" + SelectionText + "</b>" + EndingText));
                        if (isItalic) {
                            blogET.setText(Html.fromHtml(StartingText + "<b><i>" + SelectionText + "</i></b>" + EndingText));
                            if (isUnderline) {
                                blogET.setText(Html.fromHtml(StartingText + "<b><i><u>" + SelectionText + "</u></i></b>" + EndingText));
                            }
                        }
                        else if (isUnderline) {
                            blogET.setText(Html.fromHtml(StartingText + "<b><u>" + SelectionText + "</u></b>" + EndingText));
                        }
                        isBold = true;
                    }

//                        if(isBullet){
//                            String text = blogET.getText().toString();
//                            String bobo = text.replace("\n",NEWLINE_CHAR );
//                            String mText = bobo.replace(SelectionText,"<b>" + SelectionText +"</b>").replace(" ","");
//                            blogET.setText(Html.fromHtml(mText));
//
//                        }
//                        else{
//                            String text = blogET.getText().toString();
//                            if(!TextUtils.isEmpty(getSelectTV)){
//                                String cutText = text.replace(getSelectTV,"");
//                                String mText = cutText.replace(SelectionText,"<b>" + SelectionText +"</b>").replace(" ","");
//                                blogET.setText(Html.fromHtml(mText));
//                            }
//                            else{
//                                String bobo = text.replace("\n",NEWLINE_CHAR );
//                                mText = bobo.replace(SelectionText,"<b>" + SelectionText +"</b>").replace(" ","");
//                                getSelectTV = "<b>" + SelectionText + "</b>";
//                                blogET.setText(Html.fromHtml(mText));
//                            }
//                        }

                    else {
                        if (isBold) {
                            blogET.setText(Html.fromHtml(StartingText + SelectionText + EndingText));
                            if (isItalic) {
                                blogET.setText(Html.fromHtml(StartingText + "<i>" + SelectionText + "</i>" + EndingText));
                                if (isUnderline) {
                                    blogET.setText(Html.fromHtml(StartingText + "<i><u>" + SelectionText + "</u></i>" + EndingText));
                                }
                            }
                            else if (isUnderline) {
                                blogET.setText(Html.fromHtml(StartingText + "<u>" + SelectionText + "</u>" + EndingText));
                            }
                        }
                        isBold = false;
                    }
                }
                else {
                    if(SelectionText.isEmpty()) {
                        Toast.makeText(getApplicationContext(), "Please Highlight the Text!", Toast.LENGTH_LONG).show();
                        }
                }
            }
        });

        btnItalic.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                int Started = blogET.getSelectionStart();
                int Ended = blogET.getSelectionEnd();


                String StartingT = blogET.getText().toString().substring(0, Started).replaceAll("\n", NEWLINE_CHAR );
                String SelectionT = blogET.getText().toString().substring(Started, Ended).replaceAll("\n", NEWLINE_CHAR );
                String EndingT = blogET.getText().toString().substring(Ended).replaceAll("\n", NEWLINE_CHAR );

                if(!SelectionT.isEmpty()) {
                    if (!isItalic) {
                        blogET.setText(Html.fromHtml(StartingT + "<i>" + SelectionT + "</i>" + EndingT));
                        if (isBold) {
                            blogET.setText(Html.fromHtml(StartingT + "<b><i>" + SelectionT + "</b></i>" + EndingT));
                            if(isUnderline){
                                blogET.setText(Html.fromHtml(StartingT + "<b><u><i>" + SelectionT + "</b></u></i>" + EndingT));
                            }
                        }
                        else if (isUnderline) {
                            blogET.setText(Html.fromHtml(StartingT + "<u><i>" + SelectionT + "</u></i>" + EndingT));
                        }
                        isItalic = true;
                    }

                    else {
                        if (isItalic) {
                            blogET.setText(Html.fromHtml(StartingT + SelectionT + EndingT));
                            if (isBold) {
                                blogET.setText(Html.fromHtml(StartingT + "<b>" + SelectionT + "</b>" + EndingT));
                                if (isUnderline) {
                                    blogET.setText(Html.fromHtml(StartingT + "<b><u>" + SelectionT + "</b></u>" + EndingT));
                                }
                            }
                            else if (isUnderline) {
                                blogET.setText(Html.fromHtml(StartingT + "<u>" + SelectionT + "</u>" + EndingT));
                            }
                        }
                        isItalic = false;
                    }
                }
                else {
                    if(SelectionT.isEmpty()) {
                        Toast.makeText(getApplicationContext(), "Please Highlight the Text!", Toast.LENGTH_LONG).show();
                    }
                }
            }
        });

        btnUnderline.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                HashMap<String, String> blogMap = new HashMap<String, String>();

                int Start = blogET.getSelectionStart();
                int End = blogET.getSelectionEnd();
                String StartingText = blogET.getText().toString().substring(0, Start).replaceAll("\n", NEWLINE_CHAR );
                String SelectionText = blogET.getText().toString().substring(Start, End).replaceAll("\n", NEWLINE_CHAR );
                String EndingText = blogET.getText().toString().substring(End).replaceAll("\n", NEWLINE_CHAR );
                String getWholeText = StartingText + SelectionText + EndingText;



                if (!SelectionText.isEmpty()) {
                    if(!isUnderline) {
                        blogET.setText(Html.fromHtml(StartingText + "<u>" + SelectionText + "</u>" + EndingText));

                        if(isBold) {
                            blogET.setText((Html.fromHtml(StartingText + "<u><b>" + SelectionText + "</b></u>" + EndingText)));
                            if(isItalic) {
                                blogET.setText(Html.fromHtml(StartingText + "<u><b><i>" + SelectionText + "</i></b></u>" + EndingText));
                            }
                        }
                        else if(isItalic) {
                            blogET.setText(Html.fromHtml(StartingText + "<u><i>"+ SelectionText + "</i></u>" + EndingText));
                        }
                        isUnderline = true;
//                        addArray.add(getWholeText);
//                        ArrayAdapter<String> adapter = new ArrayAdapter<String>(YupaBlog.this, android.R.layout.simple_list_item_1, addArray);
//                        blogET.setText();
                    }

                    else {
                        if(isUnderline) {
                            blogET.setText(Html.fromHtml(StartingText + SelectionText + EndingText));
                            if(isBold) {
                                blogET.setText(Html.fromHtml(StartingText + "<b>" + SelectionText + "</b>" + EndingText));
                                if(isItalic) {
                                    blogET.setText(Html.fromHtml(StartingText + "<b><i>" + SelectionText + "</i></b>" + EndingText));
                                }
                            }
                        }
                        else if(isItalic) {
                            blogET.setText(Html.fromHtml(StartingText + "<i>" + SelectionText + "</i>" + EndingText));
                        }
                        isUnderline = false;
                    }
                }
                else {
                    if(SelectionText.isEmpty()) {
                        Toast.makeText(getApplicationContext(), "Please Highlight the Text!", Toast.LENGTH_LONG).show();
                    }
                }

//                blogMap.put("startingText",StartingText);
//                blogMap.put("endingText",EndingText);
//                blogMap.size();
//                blogMap.put("selectionText", "KOKYU");
//                addArray.add(blogMap);
            }
        });


       btnTextColor.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                    ColorPicker();
            }
        });

        blogET.setCustomSelectionActionModeCallback(new ActionMode.Callback() {
            @Override
            public boolean onCreateActionMode(ActionMode mode, Menu menu) {
                // Remove the "select all" option
                menu.removeItem(android.R.id.selectAll);
                // Remove the "cut" option
                menu.removeItem(android.R.id.cut);
                // Remove the "copy all" option
                menu.removeItem(android.R.id.copy);
                return true;
            }

            @Override
            public boolean onPrepareActionMode(ActionMode mode, Menu menu) {
                menu.add(0, 0, 0, "Definition").setIcon(R.drawable.ic_format_text_color);
                return true;
            }

            @Override
            public boolean onActionItemClicked(ActionMode mode, MenuItem item) {
                switch (item.getItemId()) {
                    case 0:
                        int min = 0;
                        int max = blogET.getText().length();
                        if (blogET.isFocused()) {
                            final int selStart = blogET.getSelectionStart();
                            final int selEnd = blogET.getSelectionEnd();

                            min = Math.max(0, Math.min(selStart, selEnd));
                            max = Math.max(0, Math.max(selStart, selEnd));
                        }
                        // Perform your definition lookup with the selected text
                        final CharSequence selectedText = blogET.getText().subSequence(min, max);
                        ColorPicker(selectedText);
                        // Finish and close the ActionMode
                        mode.finish();
                        return true;
                    default:
                        break;
                }
                return false;
            }

            @Override
            public void onDestroyActionMode(ActionMode mode) {

            }
        });

        btnBulletlist.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                isBullet = true;
                insertBulletPoint();
            }
        });

        btnDone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(TextUtils.isEmpty(blogTitle.getText().toString())){
                    Toast.makeText(getApplicationContext(),"Please Enter Your Blog blogTitle!", Toast.LENGTH_LONG).show();
                }
                if(TextUtils.isEmpty(blogET.getText().toString())){
                    Toast.makeText(getApplicationContext(), "Please Enter The Content!", Toast.LENGTH_LONG).show();
                }
                if(blogCategory.getSelectedItem().toString().trim().equals("Choose a blogCategory")){
                    Toast.makeText(getApplicationContext(), "Please Select The blogCategory!", Toast.LENGTH_LONG).show();
                }
                if(resultUri == null){
                    Toast.makeText(getApplicationContext(), "Please Upload An Image!", Toast.LENGTH_LONG).show();
                }
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

            if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE)
            {
                CropImage.ActivityResult result = CropImage.getActivityResult(data);
                if (resultCode == RESULT_OK) {
                    resultUri = result.getUri();
                    btnUploadIMG.setImageURI(resultUri);
                }
                else if (resultCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE)
                {
                    Exception error = result.getError();
                    Toast.makeText(this, ""+error , Toast.LENGTH_SHORT).show();
                }
            }
    }


    public void hideKeyboard(View view) {
        InputMethodManager hide = (InputMethodManager) getSystemService(Activity.INPUT_METHOD_SERVICE);
        hide.hideSoftInputFromWindow(view.getWindowToken(),0);
    }

    public void ColorPicker(final CharSequence text) {
        AmbilWarnaDialog colorPicker = new AmbilWarnaDialog(this, defaultColor, new AmbilWarnaDialog.OnAmbilWarnaListener() {
            @Override
            public void onOk(AmbilWarnaDialog dialog, int color) {
                defaultColor = color;
                int Start = blogET.getSelectionStart();
                int End = blogET.getSelectionEnd();
                String StartingText = blogET.getText().toString().substring(0, Start).replaceAll("\n", NEWLINE_CHAR );
                String SelectionText = blogET.getText().toString().substring(Start, End).replaceAll("\n", NEWLINE_CHAR );
                String EndingText = blogET.getText().toString().substring(End).replaceAll("\n", NEWLINE_CHAR );
                blogET.setText(Html.fromHtml("<font color="+"'"+defaultColor+"'"+">"+text+"</font>"));
//                blogET.setTextColor(defaultColor);

            }
            @Override
            public void onCancel(AmbilWarnaDialog dialog) {
            }
        });
        colorPicker.show();
    }


    private void insertBulletPoint() {
        ArrayList<String>allTextList = new ArrayList<>();
        ArrayList<String>selectedList = new ArrayList<>();
        int startSelection=blogET.getSelectionStart();
        int endSelection=blogET.getSelectionEnd();
        //Selected text
        String selectedText = blogET.getText().toString().substring(startSelection, endSelection);
        //This is all text
        String fullText = blogET.getText().toString();
        //Foreach loop here
        String[] splitSelectText = selectedText.split("\\s*\\r?\\n\\s*");
        for (String textSelected : splitSelectText) {
            selectedList.add(textSelected);
            Toast.makeText(getApplicationContext(),textSelected,Toast.LENGTH_SHORT).show();
        }
        String[] splitFullText = fullText.split("\\s*\\r?\\n\\s*");
        for (String allText : splitFullText) {
            //Check the selected text is equal here
            allTextList.add(allText);

            Toast.makeText(getApplicationContext(),allText,Toast.LENGTH_SHORT).show();
        }
        for (Iterator<String> iterator = allTextList.iterator(); iterator.hasNext(); ) {
            String value = iterator.next();
            if (selectedList.contains(value)) {
                iterator.remove();
            }
        }
        for(String removeBullet: selectedList){
            if(removeBullet.contains("•")){
                removeBullet.replace("•", "");
            }
        }


        allTextList.size();
        selectedList.size();



        //Variable to hold all the values
        StringBuilder output = new StringBuilder();

        for (int i = 0; i < allTextList.size(); i++) {
            //Append all the values to a string
            output.append(allTextList.get(i)).append("\n");
        }

        StringBuilder output1 = new StringBuilder();

        for (int i = 0; i < selectedList.size(); i++) {
            //Append all the values to a string
            if(selectedList.get(i).contains("•")){
                output1.append(selectedList.get(i)).toString().replace("•","");
            }
            else{
                output1.append("•").append(selectedList.get(i)).append("\n");
            }
        }

        //Set the textview to the output string
        String bobo = output.toString()+output1.toString();
        blogET.setText(bobo);





    }

    private void injectBulletPoints(String text) {
        String newLinedText = addNewLinesBetweenBullets(text);
        blogET.setText(Html.fromHtml(newLinedText));
    }

    private String addNewLinesBetweenBullets(String text) {
        return text.replaceFirst("",HTML_BULLETPOINT + " ").replaceAll("\n",NEWLINE_CHAR + HTML_BULLETPOINT + " ");
    }


}
